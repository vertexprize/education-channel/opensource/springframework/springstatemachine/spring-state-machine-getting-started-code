/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.statemachine.gs.code.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachine;
import org.springframework.statemachine.config.StateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;
import org.vertexprize.statemachine.gs.code.StateMachineLogListener;

/**
 *  Проеект и конфигурация машины состояний
 * 
 * @author developer
 */
@Configuration
@EnableStateMachine

public class StateMachineConfig  extends StateMachineConfigurerAdapter<String, String>{
 
    @Autowired
    private StateMachineLogListener stateMachineLogListener; 
    
    /**
     * Общая конфигурация машины состояний 
     * 
     * 
     * @param config
     * @throws Exception 
     */
    @Override
    public void configure(StateMachineConfigurationConfigurer<String, String> config)
            throws Exception {
        config.withConfiguration()
              .autoStartup(false)
              .machineId("state-machine-simple")
              .listener(stateMachineLogListener);
    }
    
    
    /**
     * Настройка списка состояний
     */
    
    @Override
    public void configure(StateMachineStateConfigurer<String, String> states) throws Exception {

        states.withStates()
                .initial("init")
                
                .state("S0")
                .state("S1")
                .end("finish");
               
                
                }  
  
    /**
     * Настройка переходов
     *
     *
     */
    @Override
    public void configure(StateMachineTransitionConfigurer<String, String> transitions) throws Exception {
        transitions
                .withExternal()
                    .source("init")
                    .target("S0")
                    .event("E0")
                .and()
                .withExternal()
                    .source("S0")
                    .target("S1")
                    .event("E1")
                .and()
                .withExternal()
                    .source("S1")
                    .target("finish")
                    .event("E2");
        ;
    }
    
    
    
}
